import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait

browser = webdriver.Chrome(executable_path='./drivers/chromedriver.exe')
browser.get('http://127.0.0.1:5000/')

# Add new note
button_add = browser.find_element_by_id('add')
button_add.click()
time.sleep(3)
title = browser.find_element_by_id('inputName')
title.send_keys('New note')
time.sleep(1)
content = browser.find_element_by_id('inputContent')
content.send_keys('Content for new note')
time.sleep(1)
select_importance = Select(browser.find_element_by_id('inputImportance'))
select_importance.select_by_value('5')
time.sleep(1)
save = browser.find_element_by_class_name('btn')
save.click()
time.sleep(3)

# Update new note
update = browser.find_element_by_class_name('btn-primary')
update.click()
time.sleep(3)
title = browser.find_element_by_id('inputName')
title.clear()
title.send_keys('Update Test Selenium')
time.sleep(1)
content = browser.find_element_by_id('inputContent')
content.clear()
content.send_keys('Content for Selenium Test')
time.sleep(1)
select_importance = Select(browser.find_element_by_id('inputImportance'))
select_importance.select_by_value('8')
time.sleep(1)
save = browser.find_element_by_class_name('btn')
save.click()
time.sleep(3)

# Search note
search = browser.find_element_by_class_name('form-control')
search.send_keys('ir' + Keys.ENTER)
time.sleep(3)
link_on_main_page = browser.find_element_by_id('title')
link_on_main_page.click()
time.sleep(3)

# like twice
like = browser.find_element_by_id('like')
like.click()
time.sleep(3)
like = browser.find_element_by_id('like')
like.click()
time.sleep(3)

# dislike
dislike = browser.find_element_by_id('dislike')
dislike.click()
time.sleep(3)

# delete
delete = browser.find_element_by_class_name('btn-danger')
delete.click()

